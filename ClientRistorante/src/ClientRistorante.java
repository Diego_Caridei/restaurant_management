import java.io.*;
import java.net.*;
import java.util.Scanner;
public class ClientRistorante {

    static void gestione(Integer scelta) {

        try {
            //stringa contenente il messaggio da inviare
            String line;
            String sceltaMenu;//carne o pesce
            //input da tastiera
            Scanner input;

            Boolean flag;
            flag = true;

            Boolean flag2;
            flag2 = true;
            //loopback � possibile utilizzarlo per comunicare su una stessa macchina
            String hostname = "127.0.0.1";

            //implemento un socket lato client che prende come parametri l'indirzzo 
            //del server e la porta 
            Socket client = new Socket(hostname, 5000);

            //creo un bufferReader in modo da leggere ci� che l'utente scrive
            BufferedReader in = new BufferedReader(new InputStreamReader(client.getInputStream()));
            //serve per scrivere l output stream 
            PrintWriter out = new PrintWriter(new OutputStreamWriter(client.getOutputStream()));
         

            while (true) {

                //connessione e verifica tavolo
                switch (scelta) {
                    case 1:

                        //invio 1 per chiedere la connessione e verificare
                        //se ci sono tavoli disponibili
                        line = "1";
                        out.println(line);
                        out.flush();

                        //stampo le due stringhe ricevute dal server 
                        System.out.println(in.readLine());//numero dei tavoli disponibili
                        System.out.println(in.readLine());//tavolo associato
                        //il server mi invia il flag 2 in caso di tavoli disponibili altrimenti 3
                        line = in.readLine();

                        //faccio il parsinging della stringa
                        scelta = Integer.parseInt(line);

                        break;



                    //richiesta men�    
                    case 2:

                        while (flag2) {
                            //richiedo il men�
                            out.println("2");
                            out.flush();
                            //leggo e stampo le opzioni del men�
                            System.out.println(in.readLine());
                            //ricevo la risposta se carne o pesce
                            input = new Scanner(System.in);
                            //invio la richiesta
                            sceltaMenu = input.nextLine();
                            flag = true;
                            while (flag) {
                                //se la stringa inserita dall'utente � uguale a pesce o a carne la invia al server
                                // altrimenti la richiedo
                                if (sceltaMenu.equals("carne") || sceltaMenu.equals("pesce")) {

                                    out.println(sceltaMenu);
                                    out.flush();
                                    flag = false;
                                } else {
                                    System.out.println("Richiesta non valida rinserire la scelta");
                                    sceltaMenu = input.nextLine();

                                }
                            }

                            //stampo il men�   
                            System.out.println(in.readLine());
                            System.out.println(in.readLine());
                            System.out.println(in.readLine());
                            System.out.println(in.readLine());
                            System.out.println(in.readLine());
                            System.out.println(in.readLine());
                            //confermo il men� con un si o un no
                            System.out.println("Scriva si per confermare oppure no ");
                            line = input.nextLine();



                            if (line.equals("si")) {
                                out.println(line);
                                out.flush();
                                //esce dal while 
                                flag2 = false;
                            }

                        }

                        //faccio il parsinging della stringa
                        //scelta ora vale 3 e mi sposto nel caso 3
                        //ho finito di mangiare e richiedo il conto
                        scelta = Integer.parseInt(in.readLine());


                        break;

                    case 3:
                        System.out.println("Ho finito di mangiare e pago il conto");
                        //dico al server  che pu� mandarmi il conto 
                        out.println("conto");
                        out.flush();
                        System.out.println(in.readLine());
                        out.println("pago");
                        out.flush();
                        System.out.println(in.readLine());
                        scelta = 4;
                        //faccio il parsinging della stringa
                        //scelta ora vale 4 e mi sposto nel caso 4
                        //ho finito di mangiare e richiedo il conto
                        //scelta = Integer.parseInt(in.readLine());
                        break;
                    case 4:
                        in.close();
                        out.close();
                        break;

                }

            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    public static void main(String[] args) {

        //variabile "flag"
        int risposta;

        //piccolo men�
        System.out.println("Benvenuto nel Ristorante Parthenope ");
        System.out.println("Digita uno per chiedere un tavolo");
        System.out.println();

        Scanner scelta = new Scanner(System.in);

        risposta = scelta.nextInt();


        if (risposta == 1) {
            gestione(1);

        }

        //ciclo per verificare la ric
        while (risposta != 1) {
            System.out.println("richiesta non valida ");
            System.out.println("Digita uno per chiedere un tavolo");
            System.out.println();
            risposta = scelta.nextInt();

            if (risposta == 1) {
                gestione(1);

            }

        }
    }
}
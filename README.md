#Progetto per l'esame di reti dei calcolatori: Gestione di un ristorante

![1.png](https://bitbucket.org/repo/MGpXay/images/2444356242-1.png)

#Descrizione
Si realizzi un sistema di gestione di un ristorante (SGR) che rispetti le seguenti
specifiche. Un cliente si connette, mediante un client, al server SGR
ed attende che gli venga assegnato un tavolo. Al tavolo, il cliente richiede il
menu al SGR e gli invia l’ordine. L’ SGR inoltra l’ordine alla cucina e quando
pronto, lo comunica ad un cameriere che lo serve al tavolo. Al termine
del pasto, il cliente richiede il conto al SGR ed effettua il pagamento.
Si utilizzi il linguaggio C o Java su piattaforma UNIX utilizzando i socket
per la comunicazione tra processi. Corredare limplementazione di adeguata
documentazione.
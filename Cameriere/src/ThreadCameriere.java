import java.io.*;
import java.net.*;

/**
 *
 * @author Diego Caridei
 */
public class ThreadCameriere implements Runnable {

    private Socket client;
    private String stringaRicevuta;
    private Integer idTavolo;
    private String tipoOrdine;//se carne o pesce    

    public ThreadCameriere(Socket client) {
        this.client = client;
    }

    public void run() {
        while (true) {
            try {

                //creo un BufferReade e un Printerwriter per l'input e l'output
                BufferedReader in = new BufferedReader(new InputStreamReader(client.getInputStream()));
                PrintWriter out = new PrintWriter(new OutputStreamWriter(client.getOutputStream()));

                //leggo ci� che mi 
                stringaRicevuta = in.readLine();

                if (stringaRicevuta == null) {
                    break;
                }

                //se � uguale a | sono pronto a ricevere l'ordinazione
                if (stringaRicevuta.equals("|")) {
                    //invio la stringa all ' SGR
                    out.println("Sono pronto");
                    out.flush();
                    //ricevo l'id del tavolo 
                    stringaRicevuta = in.readLine();
                    idTavolo = Integer.parseInt(stringaRicevuta);
                    System.out.println(idTavolo);
                    //il server SGR Mi invia il tipo dell 'ordine (carne o pesce)
                    tipoOrdine = in.readLine();
                    System.out.println("tipo ordine " + tipoOrdine);
                    out.println("il men� di " + tipoOrdine + " del tavolo n� " + idTavolo + " � Stato servito !!!");
                    out.flush();
                    //invio servito per far interagire 
                    out.println("servito");
                    out.flush();
                }

            } catch (IOException e) {
                e.printStackTrace();
                break;
            }
        }
    }
}

import java.io.*;
import java.net.*;
/**
 *
 * @author Diego Caridei
 */
public class Cameriere {

  
    public static void main(String[] args) throws IOException{
        
       System.out.println("Server Cameriere in attesa di connessione....");
       //in ascolto sulla porta 5002
       ServerSocket server=new ServerSocket(5002);
        while (true) {            
            try{
                
                Socket client=server.accept();
                Thread t=new Thread(new ThreadCameriere(client));
                t.start();
                
            }
            catch(IOException e){
                e.printStackTrace();
            }
        }
    }
}

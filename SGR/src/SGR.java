import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
public class SGR {

    
    public static void main(String[] args) throws IOException {

        System.out.println("SGC In attessa di connessione...");
    
        //creo una server socket che rimane in ascolto sulla porta 500
        ServerSocket server = new ServerSocket(5000);
   
            //ciclo while che attende la connessione
            while (true) {
                try {

                    //attendo una connessione di tipo client e l'accetto
                    Socket client = server.accept();

                    //passo la socket appena ottenuta alla classe ThreadServer
                    //che lavora su un thread separato
                    Thread t = new Thread(new ThreadServer(client));                                
                
                    //avvio il thread
                    t.start();

                } catch (IOException e) {
                    e.printStackTrace();
                }
                
            }
           
        }

    }
  

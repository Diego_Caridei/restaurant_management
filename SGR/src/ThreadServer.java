
import java.io.*;
import java.net.*;
import java.util.concurrent.atomic.AtomicInteger;

public class ThreadServer implements Runnable {

    //socket di tipo client
    private Socket client;
    private String stringaRicevuta;//stringa ricevuta dal client in base a quest'utlima mi muovo nei vari if
    private String line;//stringa da inviare
    private String sceltaMenu;//pesce o carnte
    static AtomicInteger tavoli = new AtomicInteger(20);

    //costruttore inizializza il socket
    public ThreadServer(Socket client) {
        this.client = client;
    }

    public void run() {

        while (true) {
            try {
                //creo un BufferReade e un Printerwriter per l'input e l'output
                BufferedReader in = new BufferedReader(new InputStreamReader(client.getInputStream()));
                PrintWriter out = new PrintWriter(new OutputStreamWriter(client.getOutputStream()));


                //leggo ci� che mi 
                stringaRicevuta = in.readLine();


                if (stringaRicevuta == null) {
                    break;
                }



                //verifico se ci sono tavoli disponibili
                if (stringaRicevuta.equals("1") && tavoli.get() >= 1) {


                    out.println("Ci sono  " + tavoli.get() + "tavoli  disponibili");
                    out.flush();

                    out.println("Il suo tavolo � il n� " + tavoli.get());
                    out.flush();

                    //invio al client il flag 2 in che lo switch entri nella seconda condizione
                    out.println("2");
                    out.flush();
                    //facci� ci� per evitare che quando i tavoli sono uguale a uno
                    //e si decrementano i tavoli entri nell if tav <= o 
                    stringaRicevuta = "";
                    tavoli.decrementAndGet();

                }


                //se ricevo uno cio� la richiesta dei tavoli 
                //e se non ci sono tavoli disponibili
                if (stringaRicevuta.equals("1") && tavoli.get() <= 0) {
                    out.println("Spiacente siamo pieni");
                    out.flush();
                    out.println("Riprovi pi� tardi");
                    out.flush();
                    //chiudo le connessioni
                    out.close();
                    in.close();
                }



                //invio il men�
                if (stringaRicevuta.equals("2")) {

                    out.println("Desidera il menu di pesce o di carne ?");
                    out.flush();



                }


                //invio il men� di carne
                if (stringaRicevuta.equals("carne")) {
                    //questa stringa sar� inviata alla cucina(scelta Menu)
                    this.sceltaMenu = "carne";
                    out.println("il menu di carne comprende :   ");
                    out.flush();
                    out.println("Antipasto : Tagliere di salumi tipici   ");
                    out.flush();
                    out.println("Primo piatto : Pappardelle ai funghi porcini ");
                    out.flush();
                    out.println("Secondo  piatto : Grigliata di carne ");
                    out.flush();
                    out.println("Dolce : Tartufo nero ");
                    out.flush();
                    out.println("Conferma l'ordinazione ? ");
                    out.flush();
                }

                //invio il men� di pesce
                if (stringaRicevuta.equals("pesce")) {
                    //questa stringa sar� inviata alla cucina (sceltaMenu)
                    this.sceltaMenu = "pesce";
                    out.println("il menu di pesce comprende :   ");
                    out.flush();
                    out.println("Antipasto : Insalata di mare   ");
                    out.flush();
                    out.println("Primo piatto : Spaghetti a vongole ");
                    out.flush();
                    out.println("Secondo  piatto : Spigola al forno ");
                    out.flush();
                    out.println("Dolce : Tartufo bianco ");
                    out.flush();
                    out.println("Conferma l'ordinazione ? ");
                    out.flush();
                }




                //conferma l'ordine e lo invio alla cucina
                if (stringaRicevuta.equals("si")) {
                    //loopback � possibile utilizzarlo per comunicare su una stessa macchina
                    String hostname = "127.0.0.1";

                    //implemento un socket lato client che prende come parametri l'indirzzo 
                    //del server e la porta 
                    Socket Cucinaclient = new Socket(hostname, 5001);

                    //creo un bufferReader in modo da leggere ci� che l'utente scrive
                    BufferedReader CucinaIn = new BufferedReader(new InputStreamReader(Cucinaclient.getInputStream()));
                    //serve per scrivere l output stream 
                    PrintWriter CucinaOut = new PrintWriter(new OutputStreamWriter(Cucinaclient.getOutputStream()));
                    //invio il flag | alla cucina per comunicargli che sono pronto 
                    line = "|";
                    CucinaOut.println(line);
                    CucinaOut.flush();
                    //mi da l'ok Sono pronto a ricevere l'ordinazione inviami l'id del tavolo e l'ordine
                    System.out.println(CucinaIn.readLine());
                    //invio il numero del tavoli
                    tavoli.getAndIncrement();
                    line = tavoli.toString();
                    CucinaOut.println(line);
                    CucinaOut.flush();
                    //invio l'ordine pesce o carne
                    CucinaOut.println(sceltaMenu);
                    CucinaOut.flush();
                    //il serverCucina mi invia la conferma che l'ordine � pronto
                    System.out.println(CucinaIn.readLine());
                    //mi invia la stringa cameriere
                    stringaRicevuta = CucinaIn.readLine();



                }


                //invio l'ordine al cemeriere 
                if (stringaRicevuta.equals("cameriere")) {
                    System.out.println("chiamo il cameriere");
                    //loopback � possibile utilizzarlo per comunicare su una stessa macchina
                    String hostname = "127.0.0.1";

                    //implemento un socket lato client che prende come parametri l'indirzzo 
                    //del server e la porta 
                    Socket CameriereClient = new Socket(hostname, 5002);

                    //creo un bufferReader in modo da leggere ci� che l'utente scrive
                    BufferedReader CameriereIn = new BufferedReader(new InputStreamReader(CameriereClient.getInputStream()));
                    //serve per scrivere l output stream 
                    PrintWriter CameriereOut = new PrintWriter(new OutputStreamWriter(CameriereClient.getOutputStream()));
                    //invio il flag | alla Cameriere per comunicargli che sono pronto 
                    line = "|";
                    CameriereOut.println(line);
                    CameriereOut.flush();
                    //stampo l'ok del cameriere
                    System.out.println(CameriereIn.readLine());
                    //Invio la scelta del men� e il numero del tavolo al cameriere
                    //invio il numero del tavolo
                    line = tavoli.toString();
                    CameriereOut.println(line);
                    CameriereOut.flush();
                    //invio l'ordine pesce o carne
                    CameriereOut.println(sceltaMenu);
                    CameriereOut.flush();
                    //il serverCameriere  mi invia la conferma che l'ordine � pronto
                    System.out.println(CameriereIn.readLine());
                    //il cameriere mi invier� servito 
                    stringaRicevuta = CameriereIn.readLine();

                }

                //contatto il cliente per sapere se vuole il conto 
                if (stringaRicevuta.equals("servito")) {
                    //sono in servito 
                    System.out.println("sono in servito ");
                    //invio 3 al client per far entrare in modali�" finito di mangiare"
                    out.println("3");
                    out.flush();

                }

                if (stringaRicevuta.equals("conto")) {
                    System.out.println("sono in conto ");
                    if (sceltaMenu.equals("carne")) {
                        out.println("il conto per il menu di " + sceltaMenu + " � di 30 euro");
                        out.flush();
                    } else {
                        out.println("il conto per il menu di " + sceltaMenu + " � di 40 euro");
                        out.flush();
                    }

                }


                if (stringaRicevuta.equals("pago")) {
                    System.out.println("sono in pago ");

                    out.println("Arrivederci");
                    out.flush();
                    //incremento i tavoli in quanto si � liberato uno
                    tavoli.incrementAndGet();

                    //chiudo le connessioni 
                    in.close();
                    out.close();
                }

            } catch (IOException e) {
                e.printStackTrace();
                break;
            }
        }

    }
}

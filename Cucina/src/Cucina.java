
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 *
 * @author Diego Caridei
 */
public class Cucina {

  
    public static void main(String[] args) throws IOException{
        
       System.out.println("Server Cucina in attesa di connessione....");
       //in ascolto sulla porta 5001
       ServerSocket server=new ServerSocket(5001);
        while (true) {            
            try{
                
                Socket client=server.accept();
                Thread t=new Thread(new ThreadCucina(client));
                t.start();
                
            }
            catch(IOException e){
                e.printStackTrace();
            }
        }
    }
}

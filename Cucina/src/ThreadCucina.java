import java.net.*;
import java.io.*;

/**
 *
 * @author Diego Caridei
 */
public class ThreadCucina implements Runnable {

    private Socket client;
    private String stringaRicevuta;
    private Integer idTavolo;
    private String tipoOrdine;//se carne o pesce
    //costruttore inizializza il socket

    public ThreadCucina(Socket client) {
        this.client = client;
    }

    public void run() {
        while (true) {
            try {
                //creo un BufferReade e un Printerwriter per l'input e l'output
                BufferedReader in = new BufferedReader(new InputStreamReader(client.getInputStream()));
                PrintWriter out = new PrintWriter(new OutputStreamWriter(client.getOutputStream()));

                //leggo ci� che mi 
                stringaRicevuta = in.readLine();


                //se � uguale a | sono pronto a ricevere l'ordinazione
                if (stringaRicevuta.equals("|")) {
                    //invio la stringa all ' SGR
                    out.println("Sono pronto a ricevere l'ordinazione inviami l'id del tavolo e l'ordine");
                    out.flush();
                    //ricevo l'id del tavolo 
                    stringaRicevuta = in.readLine();
                    idTavolo = Integer.parseInt(stringaRicevuta);
                    System.out.println(idTavolo);
                    //il server SGR Mi invia il tipo dell 'ordine (carne o pesce)
                    tipoOrdine = in.readLine();
                    System.out.println("tipo ordine " + tipoOrdine);
                    out.println("il men� di " + tipoOrdine + " del tavolo n� " + idTavolo + " � pronto");
                    out.flush();
                    out.println("cameriere");
                    out.flush();
                }

                if (stringaRicevuta == null) {
                    break;
                }
            } catch (IOException e) {
                e.printStackTrace();
                break;
            }
        }
    }
}
